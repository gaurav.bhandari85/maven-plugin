package com.gaurav.example;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.ProgressHandler;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ProgressMessage;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

// Docker Client Images

/**
 * An example Maven Mojo that resolves the current project's git revision and adds that a new {@code exampleVersion}
 * property to the current Maven project.
 */
@Mojo(name = "version", defaultPhase = LifecyclePhase.INITIALIZE)
public class SimpleMojo extends AbstractMojo {


    /**
     * The git command used to retrieve the current commit hash.
     */
    //@Parameter(property = "git.command", defaultValue = "git status")
    //@Parameter(property = "git.command", defaultValue = "git rev-parse --short HEAD")
    //private String command;

    @Parameter(property = "project", readonly = true)
    private MavenProject project;

    @Parameter(property = "folder", readonly = false, defaultValue = "docker")
    private String folder;

    @Parameter(property = "imagename", readonly = false, defaultValue = "docker")
    private String imagename;


    public void execute() throws MojoExecutionException, MojoFailureException {
        //String version = getVersion(command);
        //project.getProperties().put("exampleVersion", version);
        //getLog().info("Git hash: " + version);

        /*
        try {
            getLog().info("Building Client");
            DockerClient docker = DefaultDockerClient.fromEnv().build();
            getLog().info("Built");
            // Bind container ports to host ports
            final String[] ports = {"2375"};
            final Map<String, List<PortBinding>> portBindings = new HashMap<>();
            for (String port : ports) {
                List<PortBinding> hostPorts = new ArrayList<>();
                hostPorts.add(PortBinding.of("0.0.0.0", port));
                portBindings.put(port, hostPorts);
            }
            final HostConfig hostConfig = HostConfig.builder().portBindings(portBindings).build();
            // Create container with exposed ports
            final ContainerConfig containerConfig = ContainerConfig.builder()
                    .hostConfig(hostConfig)
                    .image("busybox").exposedPorts(ports)
                    .cmd("sh", "-c", "while :; do sleep 1; done")
                    .build();
            final ContainerCreation creation = docker.createContainer(containerConfig);
            final String id = creation.id();

        } catch (DockerCertificateException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (DockerException e) {
            e.printStackTrace();
        }
        */


        final AtomicReference<String> imageIdFromMessage = new AtomicReference<>();

        URI dockerDirectory = null;

        final DockerClient docker;

        final String returnedImageId;
        try {

            //dockerDirectory = new java.net.URI(url);
            docker = DefaultDockerClient.fromEnv()
                    .connectionPoolSize(200)
                    .build();
            getLog().info("PATH for docker file:" + folder);
            getLog().info("Name of Docker Image:" + imagename);

            getLog().info("STARTING to BUILD");
            returnedImageId = docker.build((Paths.get(folder)
                    ), imagename, new ProgressHandler() {
                        @Override
                        public void progress(ProgressMessage message) throws DockerException {

                            final String imageId = message.buildImageId();
                            if (imageId != null) {
                                imageIdFromMessage.set(imageId);
                            }
                        }
                    });
            getLog().info("Built Image,ID:" + returnedImageId);
        } catch (DockerException e) {
            getLog().info("Error::" + e.getMessage());
            e.printStackTrace();

        } catch (InterruptedException e) {
            getLog().info("Error::" + e.getMessage());
            e.printStackTrace();
        } catch (IOException | DockerCertificateException e) {
            getLog().info("Error::" + e.getMessage());
            e.printStackTrace();
        }


    }

    private String getVersion(String command) throws MojoExecutionException {
        try {
            StringBuilder builder = new StringBuilder();

            Process process = Runtime.getRuntime().exec(command);
            Executors.newSingleThreadExecutor().submit(() ->
                    new BufferedReader(new InputStreamReader(process.getInputStream())).lines().forEach(builder::append)
            );
            int exitCode = process.waitFor();

            if (exitCode != 0) {
                throw new MojoExecutionException("Execution of command '" + command + "' failed with exit code: " + exitCode);
            }

            // return the output
            return builder.toString();

        } catch (IOException | InterruptedException e) {
            throw new MojoExecutionException("Execution of command '" + command + "' failed", e);
        }
    }
}
